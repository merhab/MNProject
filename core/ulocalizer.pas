unit ulocalizer;

interface

uses
  utables, ufrmCon, Aurelius.Engine.ObjectManager,
  Aurelius.Engine.DatabaseManager, System.SysUtils, System.Classes,Aurelius.Criteria.Linq;

type
  TMLocalizer = class
  private
    objMan: TObjectManager;
    Flang: string;
  public
    property lang: string read Flang write Flang;
    constructor Create; overload;
    constructor Create(lang: string); overload;
    destructor Destroy;override;
    procedure regester(text: WideString);
    function getTranslation(text: WideString): WideString;
    function localize(text: WideString): WideString;
  end;

function TR(text: WideString): WideString; overload;

function TR(text, lang: WideString): WideString; overload;

implementation

{ TMLocalizer }

uses
 uconst;

constructor TMLocalizer.Create;
begin
  objMan := frmCon.createObjectManagerLocalParams;
  self.lang := objMan.Find<TMParamsLocal>(1).Language;
end;

constructor TMLocalizer.Create(lang: string);
begin
  Self.lang := lang;
  objMan := frmCon.createObjectManagerLocalParams;
end;

destructor TMLocalizer.Destroy;
begin
  Self.objMan.Free;
  inherited Destroy;
end;

function TMLocalizer.getTranslation(text: WideString): WideString;
begin
  var word := objMan.Find<TMWrod>.Where(linq['Word'] = text).UniqueResult;
  if word = nil then
  try
    regester(text);
  except
    raise;
  end;
  var trans := objMan.Find<TMTrans>.add(linq['Lang.Lang'] = lang).add
    (linq['Word.Word'] = text).UniqueResult;
  if trans <> nil then
  begin
    Result := trans.Trans;
  end
  else
  begin
    trans := TMTrans.Create(text, objMan.Find<TMLang>.where(linq['Lang']
      = lang).UniqueResult, word);
    objMan.AddOwnership(trans);
    try
      objMan.Save(trans);
    except
      raise;
    end;
    Result := text;
  end;
end;

function TMLocalizer.localize(text: WideString): WideString;
begin
  try
    Result := getTranslation(text);
  except
    raise;
  end;
end;

procedure TMLocalizer.regester(text: WideString);
var
  word: TMWrod;
  language: TMLang;
  trans: TMTrans;
begin
  word := objMan.Find<TMWrod>.Where(linq['Word'] = text).UniqueResult;
  if word = nil then
  begin
    word := TMWrod.Create(text);
    objMan.AddOwnership(word);
    language := objMan.Find<TMLang>.Where(linq['Lang'] =
      TRANS_DEFAULT_LANG).UniqueResult;
    trans := TMTrans.Create(text, language, word);
    objMan.AddOwnership(trans);
    try
      objMan.Save(trans);
    except
      on e: exception do
        raise Exception.Create(TR(ERR_CANT_SAVE_RECORD) + N_LINE + e.ToString);

    end;

  end;

end;


function TR(text: WideString): WideString;
begin
  var localizer := TMLocalizer.Create;
  try
    result := localizer.localize(text);
  finally
    localizer.Free;
  end;

end;

function TR(text, lang: WideString): WideString;
begin
  var localizer := TMLocalizer.Create(lang);
  try
    result := localizer.localize(text);
  finally
    localizer.Free;
  end;

end;

end.

//memory chcekced


