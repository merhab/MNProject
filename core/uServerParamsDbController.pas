unit uServerParamsDbController;

interface

uses
  uConnection,
  System.Classes,
  System.SysUtils,
  uconst,
  uDbManager,
  uDatabase,
  mORMot;

const
  DB_DRIVER = 'mysql';
  DB_SERVER = 'localhost';
  SERVER_PORT = '0';
  USER_NAME = 'root';
  PASS_WORD = '';
  DB_NAME = 'params';
  CLASSES_ARRAY: array of TSQLRecordClass = [TMDatabases];

type
  TMServerParamsDbCntr = class(TComponent)
  private
    con: TMConnection;
  public
    constructor Create(AOwner: TComponent); override;
    class procedure createServerParamsDataBase;
    procedure updateDbStructure;
    procedure createDefaultDb;
    function createDbManager(aOwner: TComponent = nil): TMDBManager;
    function connect: Boolean;
    class function getActiveDbNAme: string;
  end;

implementation

{ TMServerParamsDbCntr }

function TMServerParamsDbCntr.connect: Boolean;
begin
  con := TMConnection.Create(Self, DB_DRIVER, DB_SERVER, SERVER_PORT, USER_NAME,
    PASS_WORD, DB_NAME);
end;

constructor TMServerParamsDbCntr.Create(AOwner: TComponent);
begin
  inherited;
  con := TMConnection.Create(Self, DB_DRIVER, DB_SERVER, SERVER_PORT, USER_NAME,
    PASS_WORD, DB_NAME);
  try
    con.open;
  except
    on e: Exception do
      raise Exception.Create('Cant Open ' + DB_NAME + N_LINE + e.ToString);
  end;
end;

function TMServerParamsDbCntr.createDbManager(aOwner: TComponent): TMDBManager;
begin
  Result := TMDBManager.Create(aOwner, con, CLASSES_ARRAY);
end;

procedure TMServerParamsDbCntr.createDefaultDb;
begin
  var db := TMDatabases.Create('DEFAULT');
  db.isDefault := True;
  var dbMan := createDbManager(nil);
  try
    dbMan.saveIfNOtExists(@db, 'title =?', ['DEFAULT']);
  finally
    dbMan.Free;
  end;
end;

class procedure TMServerParamsDbCntr.createServerParamsDataBase;
begin
  var con := TMConnection.Create(nil, DB_DRIVER, DB_SERVER, SERVER_PORT,
    USER_NAME, PASS_WORD, '');
  try
    try
      con.open;
      con.execute('CREATE DATABASE IF NOT EXISTS ' + DB_NAME);
    except
      on e: Exception do
        raise Exception.Create('Cant create databse ' + DB_NAME + N_LINE + e.ToString);
    end;
  finally
    con.Free;
  end;
end;

class function TMServerParamsDbCntr.getActiveDbNAme: string;
begin
  var con := TMConnection.Create(nil, DB_DRIVER, DB_SERVER, SERVER_PORT,
    USER_NAME, PASS_WORD, DB_NAME);
  try
    con.open;
    var man := TMDBManager.Create(nil, con, CLASSES_ARRAY);
    try
      var db := TMDatabases.CreateAndFillPrepare(man.fClient, 'isDefault =?', [true]);
      result :=db.name;
      db.Free;
    finally
      man.Free;
    end;
  finally
    con.Free;
  end;
end;

procedure TMServerParamsDbCntr.updateDbStructure;
begin
  var dbMan := TMDBManager.Create(nil, con, CLASSES_ARRAY);
  try
    try
      dbMan.updateDatabase;
    except
      on e: Exception do
        raise Exception.Create('Cant update  databse structure fo database:' +
          DB_NAME + N_LINE + e.ToString);
    end;
  finally
    dbMan.Free;
  end;
end;

end.

