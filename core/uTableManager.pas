unit uTableManager;

interface

uses
  mORMot,
  SynDBUniDAC,
  SynCommons,
  mORMotDB,
  mORMotSQLite3,
  SynDB,
  System.Classes,
  System.SysUtils,
  Vcl.Dialogs,
  uConnection,
  System.Generics.Collections;

type
  tCallBack= procedure(obj:TSQLRecord);

  TMTableManager = class(TComponent)
  private
    aModel: TSQLModel;
    aClient: TSQLRestClientDB;
    aCachData: TArray<TSQLRecord>;
    aCach: TDynArray;
    aIsCachEnabled: Boolean;
    claz:TSQLRecordClass;
  public
    constructor Create(AOwner: TComponent; conn: TMConnection; claz:
      TSQLRecordClass; isCachEnabled: Boolean = True);
    destructor Destroy; override;
    procedure addToCach(rd: TSQLRecord);
    function save(rd: TSQLRecord): TID;
    function getRecordByID<T: TSQLRecord>(Id: Integer): T;
    function update(rec: TSQLRecord): Boolean;
    function Delete<T: TSQLRecord>(rec: T): Boolean;

  end;

implementation

{ TMTableManager }

procedure TMTableManager.addToCach(rd: TSQLRecord);
begin
  if (aCach.find(rd) = -1) then
    aCach.Add(rd);
end;

constructor TMTableManager.Create(AOwner: TComponent; conn: TMConnection; claz:
  TSQLRecordClass; isCachEnabled: Boolean = True);
begin
  inherited Create(AOwner);
  aCach.Init(TypeInfo(TArray<TSQLRecord>), aCachData);
  aModel := TSQLModel.Create([claz]);
  VirtualTableExternalRegister(aModel, claz, conn.getConnectionProp, '');
  aClient := TSQLRestClientDB.Create(aModel, nil, ':memory:', TSQLRestServerDB);
  aIsCachEnabled := isCachEnabled;
   claz:= claz;
end;

function TMTableManager.Delete<T>(rec: T): Boolean;
begin
  Result := aClient.Delete(T, rec.ID)
end;

destructor TMTableManager.Destroy;
begin
  aModel.Free;
  aClient.Free;
  ObjArrayClear(aCach);
  inherited;
end;

function TMTableManager.getRecordByID<T>(Id: Integer): T;
begin
  Result := T.create;
  aCach.Add(Result);
  if not aClient.Retrieve(Id, Result) then
    Result := nil;

end;



function TMTableManager.save(rd: TSQLRecord): TID;
begin
  Result := aClient.Add(rd, True);
  if aIsCachEnabled then
    aCach.Add(rd);
end;

function TMTableManager.update(rec: TSQLRecord): Boolean;
begin
  Result := aClient.Update(rec);
end;

end.

