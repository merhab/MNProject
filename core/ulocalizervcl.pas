unit ulocalizervcl;

interface

uses
  Vcl.StdCtrls, Vcl.Controls, Vcl.Forms, ulocalizer, System.Classes,
  vcl.ActnList, vcl.Menus;

type
  TLocalizerVCL = class
  public
    class procedure localize(ctrl: TComponent; var localizer: TMLocalizer); overload;
    class procedure localize(ctrl: TComponent); overload;
    class procedure localize(ctrl: TComponent; lang: string); overload;
    class procedure regester(ctrl: TComponent; var localizer: TMLocalizer);

  end;

implementation

{ TLocalizerDevExpress }

class procedure TLocalizerVCL.localize(ctrl: TComponent; var localizer: TMLocalizer);
var
  i: Integer;
begin
    if (ctrl is TLabel) then
      TLabel(ctrl).Caption := localizer.localize(TLabel(ctrl).Caption)
    else if ctrl is TForm then
      TForm(ctrl).Caption := localizer.localize(TForm(ctrl).Caption)
    else if ctrl is TButton then
      TButton(ctrl).Caption := localizer.localize(TButton(ctrl).Caption)
    else if ctrl is TAction then
      TAction(ctrl).Caption := localizer.localize(TAction(ctrl).Caption);
  for i := ctrl.ComponentCount - 1 downto 0 do
  begin
    var Temp := ctrl.Components[i];
    if Temp is TComponent then
      localize(TComponent(Temp), localizer);

  end;

end;

class procedure TLocalizerVCL.localize(ctrl: TComponent);
begin
  var localiser := TMLocalizer.Create;
  try
    localize(ctrl, localiser);
  finally
    localiser.Free;
  end;

end;

class procedure TLocalizerVCL.localize(ctrl: TComponent; lang: string);
begin
  var localiser := TMLocalizer.Create(lang);
  try
    localize(ctrl, localiser);
  finally
    localiser.Free;
  end;

end;

class procedure TLocalizerVCL.regester(ctrl: TComponent; var localizer: TMLocalizer);
var
  i: Integer;
begin
  if (ctrl is TLabel) then
    localizer.regester(TLabel(ctrl).Caption)
  else if ctrl is TForm then
    localizer.regester(TForm(ctrl).Caption)// := localizer.localize(TForm(ctrl).Caption)
  else if ctrl is TButton then
    localizer.regester(TButton(ctrl).Caption)// := localizer.localize(TButton(ctrl).Caption)
 else if ctrl is TAction then
    localizer.regester(TAction(ctrl).Caption);

  for i := ctrl.ComponentCount - 1 downto 0 do
  begin
    var Temp := ctrl.Components[i];
    if Temp is TComponent then
      regester(TComponent(Temp), localizer);

  end;
end;

end.

