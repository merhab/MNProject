unit uDbManager;

interface

uses
  mORMot,
  SynDBUniDAC,
  SynCommons,
  mORMotDB,
  mORMotSQLite3,
  SynDB,
  System.Classes,
  System.SysUtils,
  Vcl.Dialogs,
  uConnection,
  System.Generics.Collections,
  System.Contnrs,
  System.UITypes,
  System.Rtti;

type
  tCallBack = procedure(obj: TSQLRecord);

  PTSQlRecord = ^TSQLRecord;

  TMDBManager = class(TComponent)
  private
    aModel: TSQLModel;
    //classesArr: array of TSQLRecordClass;

    Fconn: TMConnection;
    //aCachData: TArray<TSQLRecord>;
    aCachSqlTables: TObjectList<TSQLTable>;
    aCach: TObjectList<TSQLRecord>;
    fclazes: array of TSQLRecordClass;
  public
    fClient: TSQLRestClientDB;
    constructor Create(AOwner: TComponent; conn: TMConnection; clazes: array of
      TSQLRecordClass); reintroduce;
    destructor Destroy; override;
    procedure updateDatabase;
    procedure addToCach(rd: TSQLRecord);
    function save(rd: TSQLRecord; isCached: Boolean = True): TID;
    function saveIfNOtExists(rd: PTSQlRecord; aSQl: RawUTF8; params: array of
      const; isCached: Boolean = True): TID;
    function update(rec: TSQLRecord): Boolean;
    function Delete(rec: TSQLRecord): Boolean; overload;
    function Delete(claz: TSQLRecordClass; Id: TID): Boolean; overload;
    function getRecordByID<T: TSQLRecord>(Id: Integer; isCached: Boolean = True): T;
    procedure ExecCallBack(claz: TSQLRecordClass; aSQl: RawUTF8; params: array
      of const; callBack: tCallBack = nil; fields: RawUTF8 = '');
    function CreateRecList(claz: TSQLRecordClass; aSQl: RawUTF8; params: array
      of const; fields: RawUTF8 = ''; isCached: Boolean = True): TList<TSQLRecord>;
    function CreateSqLTableJson(claz: TSQLRecordClass; aSQl: RawUTF8; params:
      array of const; fields: RawUTF8 = ''; isCached: Boolean = True): TSQLTableJSON;
    function CreateObjectList(claz: TSQLRecordClass; aSQl: RawUTF8; params:
      array of const; fields: RawUTF8 = ''; isCached: Boolean = True): TObjectList;
  end;

implementation

{ TMDBManager }

uses
  mORMotMidasVCL;

procedure TMDBManager.addToCach(rd: TSQLRecord);
begin
  //if not aCach.Contains(rd) then
  aCach.Add(rd);
end;

destructor TMDBManager.Destroy;
begin
  aModel.Free;
  fClient.Free;
  aCach.Free;
  aCachSqlTables.Free;
  inherited;
end;

constructor TMDBManager.Create(AOwner: TComponent; conn: TMConnection; clazes:
  array of TSQLRecordClass);
begin
  inherited Create(AOwner);
  aCach := TObjectList<TSQLRecord>.Create;
  aCachSqlTables := TObjectList<TSQLTable>.Create;
  try
    aModel := TSQLModel.Create(clazes);
  except
    on e: Exception do
      raise Exception.Create(e.ToString);

  end;
  var claz: TSQLRecordClass;
  for claz in clazes do
  begin
    VirtualTableExternalRegister(aModel, claz, conn.getConnectionProp, '');
  end;
  fClient := TSQLRestClientDB.Create(aModel, nil, ':memory:', TSQLRestServerDB);
  fClient.Server.CreateMissingTables;
  Fconn := conn;
  SetLength(fclazes, Length(clazes));
  var i: Integer;
  for i := 0 to Length(clazes) - 1 do
    Self.fclazes[i] := clazes[i];

end;

function TMDBManager.CreateObjectList(claz: TSQLRecordClass; aSQl: RawUTF8;
  params: array of const; fields: RawUTF8; isCached: Boolean): TObjectList;
begin
  Result := fClient.RetrieveList(claz, aSQl, params, fields);
  if isCached then
  begin
    var rec: TSQLRecord;
    for rec in Result do
      addToCach(rec);
  end;
end;

function TMDBManager.Delete(rec: TSQLRecord): Boolean;
begin
  Result := fClient.Delete(rec.RecordClass, rec.GetID);
end;

function TMDBManager.Delete(claz: TSQLRecordClass; id: TID): Boolean;
begin
  Result := fClient.Delete(claz, id);
end;

function TMDBManager.CreateRecList(claz: TSQLRecordClass; aSQl: RawUTF8; params:
  array of const; fields: RawUTF8; isCached: Boolean): TList<TSQLRecord>;
begin
  Result := TList<TSQLRecord>.Create;
  var FoundRec := claz.CreateAndFillPrepare(fClient, aSQl, params, fields);

  while FoundRec.FillOne do
  begin
    Result.Add(FoundRec);
  end;
  if isCached then
  begin
    var rec: TSQLRecord;
    for rec in Result do
      addToCach(rec);
  end;
end;

function TMDBManager.CreateSqLTableJson(claz: TSQLRecordClass; aSQl: RawUTF8;
  params: array of const; fields: RawUTF8; isCached: Boolean): TSQLTableJSON;
begin
  Result := fClient.MultiFieldValues(claz, fields, aSQl, params);
  if isCached then
  begin
    aCachSqlTables.Add(Result);
  end;
end;

function TMDBManager.getRecordByID<T>(Id: Integer; isCached: Boolean): T;
begin
  Result := T.create;
  if isCached then
    aCach.Add(Result);
  if not fClient.Retrieve(Id, Result) then
    Result := nil;
end;

function TMDBManager.save(rd: TSQLRecord; isCached: Boolean): TID;
var
  LContext: TRttiContext;
  LType: TRttiType;
  LMethod: TRttiMethod;
  LProperty: TRttiProperty;
  LField: TRttiField;
var
  clz: TSQLRecordClass;
begin
  LContext := TRttiContext.Create;
  try
    LType := LContext.GetType(rd.ClassInfo);

    for LField in LType.GetFields do
    begin
      if LField.GetValue(rd).IsInstanceOf(TSQLRecord) then
        for clz in Self.fClazes do
        begin
          if LField.GetValue(rd).IsInstanceOf(clz) then
          begin
            var obj := (LField.GetValue(rd).AsObject) as clz;
            if obj.GetID = 0 then
              Self.save(obj);
            obj := obj.AsTSQLRecord;
            var adr := rd.FieldAddress(LField.Name);
            if adr = nil then
            begin
              raise Exception.Create('Field:' + LField.Name + ' from class:' +
                clz.ClassName + ' MUST BE PUBLISHED');
            end;

            Pointer(adr^) := obj;
          end;

        end;
    end;
  finally
    LContext.Free;
  end;

  if isCached then
    addToCach(rd);
  Result := Self.fClient.Add(rd, True);
end;

function TMDBManager.saveIfNOtExists(rd: PTSQlRecord; aSQl: RawUTF8; params:
  array of const; isCached: Boolean): TID;
begin
  var cls: TSQLRecordClass;
  for cls in Self.fclazes do
    if rd^ is cls then
    begin
      var obj := cls.Create(fClient, aSQl, params);
      var n := obj.GetID;
      try
        if obj.GetID > 0 then
        begin
          rd^.IDValue := obj.GetID;
          if isCached then
            addToCach(rd^);
        end
        else
        begin
          save(rd^, isCached);
        end;

      finally
        obj.Free;
      end;
      Break;
    end;
//  if isCached then
//    addToCach(rd^);
//  rd.FillPrepare(fClient, aSQl, params);
//  if not rd.FillOne then
//    save(rd^, false)

end;

function TMDBManager.update(rec: TSQLRecord): Boolean;
begin
  Result := Self.fClient.Update(rec);
end;

procedure TMDBManager.updateDatabase;
begin
  if Fconn.Active then
  begin
    try
      fClient.Server.CreateMissingTables
    except
      on e: Exception do
        MessageDlg(e.toString, mtError, [mbOK], 0);
    end;
  end
  else
    raise Exception.Create('the connection is closed');
end;

procedure TMDBManager.ExecCallBack(claz: TSQLRecordClass; aSQl: RawUTF8; params:
  array of const; callBack: tCallBack; fields: RawUTF8);
begin
  var FoundMovie := claz.CreateAndFillPrepare(fClient, aSQl, params, fields);
  try
    while FoundMovie.FillOne do
    begin
      if Assigned(callBack) then
        callBack(FoundMovie);
    end;
  finally
    FoundMovie.Free;
  end;
end;

end.

