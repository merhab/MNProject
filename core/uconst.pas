unit uconst;

interface

uses
  System.Generics.Collections;

const
//*****************names******************
  DB_FILE_EXT = '.db';
  APPDATA_DIR = 'merhab';
  DB_LOCAL_PARAMS_NAME = 'paramsLocal';
  DB_SERVER_PARAMS_NAME = 'paramsServer';
  DB_DEFAULT_NAME = 'DEFAULT_DB';
  TRANS_DEFAULT_LANG = 'EN';
  TRANS_ARABIC_LANG = 'AR';
  TRANS_FRENSH_LANG = 'FR';
  GROUP_SUPER_NAME = 'ADMIN_GROUP';
  USER_SUPER_NAME = 'ADMIN_NAME';
  N_LINE = AnsiString(#13#10);
//***************ERRORS**********
  ERR_NO_ACTIVE_DB = 'there is no active database';
  ERR_NO_PERMISSION = 'you don`t have permisssion; please reanter a correct name/password';
  ERR_WRONG_DB_NAME = 'DATABASE NAME IS WROMG';
  ERR_EXISTS_DB_NAME = 'DATABASE NAME EXISTS,TRY ANOTHER ONE';
  ERR_CANT_DELETE_THE_ONLY_DB = 'YOU CANT DELETE THE ONLY EXISTING DATABASE';
  ERR_CANT_SAVE_RECORD = 'WE CANT SAVE THE OBJECT ';
  ERR_CANT_CREATE_DIR = 'CANT CREATE DIR';
  ERR_CANT_CONNECT_TO_SERVER = 'CAN NOT CONNECT TO SERVER';
  ERR_CANT_CREATE_DB_SERVER = 'CAN NOT CREATE DB SERVER';
  ERR_USER_NAME='USER NAME MUST HAS A VALUE';
 //*************MESSAGES*********
  MSG_NEW_DB = 'NEW DATABASE';
  MSG_GIVE_DB_NAME = 'PLEASE GIVE THE NAME OF YOUR DATABASE';
  MSG_DELETE_CONFIRMATION = 'ARE YOU SURE YOU WANT DELETE THIS DATABASE?';
  MSG_IF_SURE_WRITE_NAME = 'PLEASE WRITE DATABASE NAME NEXT TO CONFIRM';
  MSG_DATA_NEED_BE_SAVED = 'THERE IS DATA NEED TO BE SAVED' + N_LINE + 'CLOSE ANY WAY?';
  MSG_CONFIRM_DELETE_RECORD = 'ARE YOU SURE YOU WANT DELETE THIS RECORD?';
  MSG_DATA_MAY_LOST = 'DATA CAN BE LOST';

var
  MsgList: TList<string>;
  autoInc: Integer;

procedure init;

function Add(text: string): string;

implementation

procedure init;
begin
  MsgList := Tlist<string>.create;
  MsgList.Add(ERR_NO_ACTIVE_DB);
  MsgList.Add(ERR_NO_PERMISSION);
  MsgList.Add(MSG_NEW_DB);
  MsgList.Add(MSG_GIVE_DB_NAME);
  MsgList.Add(ERR_WRONG_DB_NAME);
  MsgList.Add(ERR_EXISTS_DB_NAME);
  MsgList.Add(MSG_DELETE_CONFIRMATION);
  MsgList.Add(MSG_IF_SURE_WRITE_NAME);
  MsgList.Add(ERR_CANT_DELETE_THE_ONLY_DB);
  MsgList.Add(ERR_CANT_SAVE_RECORD);
  MsgList.Add(MSG_DATA_NEED_BE_SAVED);
  MsgList.Add(MSG_CONFIRM_DELETE_RECORD);
  MsgList.Add(MSG_DATA_MAY_LOST);
  MsgList.Add(ERR_CANT_CREATE_DIR);
  MsgList.Add(ERR_CANT_CONNECT_TO_SERVER);
  MsgList.Add(ERR_CANT_CREATE_DB_SERVER);
  MsgList.Add(ERR_USER_NAME);
end;

function add(text: string): string;
begin
  MsgList.add(text);
  result := text;
end;

initialization
  autoInc := 0;
  init;


finalization
  MsgList.Free;


end.

