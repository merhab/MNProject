program MNProject;

uses
  Vcl.Forms,
  ufrmLogin in 'forms\ufrmLogin.pas' {frmLogin},
  ufrmCon in 'frames\ufrmCon.pas' {frmCon: TDataModule},
  uconst in 'core\uconst.pas',
  utables in 'tables\utables.pas',
  ufrmLoginCtrl in 'forms\ufrmLoginCtrl.pas',
  ulocalizer in 'core\ulocalizer.pas',
  ulocalizervcl in 'core\ulocalizervcl.pas',
  uglobalLogin in 'core\uglobalLogin.pas',
  uConnection in 'core\uConnection.pas',
  uDbManager in 'core\uDbManager.pas',
  uTableManager in 'core\uTableManager.pas',
  uDatabase in 'tables\uDatabase.pas',
  ufrmLogin2 in 'forms\ufrmLogin2.pas' {frmLogin2},
  uServerParamsDbController in 'core\uServerParamsDbController.pas',
  uDbController in 'core\uDbController.pas';

{$R *.res}

begin
  {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}
  Application.Initialize;

  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmLogin2, frmLogin2);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmCon, frmCon);
  Application.Run;
end.

