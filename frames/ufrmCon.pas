unit ufrmCon;

interface

uses
  Aurelius.Drivers.Interfaces, Aurelius.Drivers.UniDac, System.SysUtils,
  System.Classes, Aurelius.Sql.MySQL, Aurelius.Schema.MySQL,
  Aurelius.Comp.Connection, Data.DB, DBAccess, Uni, UniProvider,
  MySQLUniProvider, Aurelius.Sql.Sqlite, Aurelius.Schema.Sqlite,
  Aurelius.Events.Manager, SQLiteUniProvider, Aurelius.Comp.ModelEvents,
  System.IOUtils, Aurelius.Engine.DatabaseManager, Aurelius.Mapping.Explorer,
  Aurelius.Linq, Aurelius.Engine.ObjectManager,
  utables, Aurelius.Criteria.Base;

type
  TMObjectManager = class helper for TObjectManager
    function saveIfNotExists<T: MTable, constructor>(var obj: T; cretiria: TCustomCriterion): Integer;
    procedure AddOwnershipWithSlaves<T: MTable>(var obj: T); overload;
  end;

  TBooleanMsg = record
    success: Boolean;
    message: WideString;
  end;

  TfrmCon = class(TDataModule)
    conServer: TUniConnection;
    AurConServer: TAureliusConnection;
    aurConLocal: TAureliusConnection;
    events: TAureliusModelEvents;
    conLocal: TUniConnection;
    unsql: TUniSQL;
  public

    class function canConnectToServer: TBooleanMsg; static;

    class procedure closeServer; static;
    class procedure openLocal; static;
    class procedure openServer; static;
    class function createDbManagerServerParams: TDatabaseManager; static;
    class procedure createDbServer(database: string); static;
    class procedure createDbServerParams; static;
    class function CreateConnectionServer: IDBConnection; static;
    class function createDbManagerLocalParams: TDatabaseManager; static;
    class function createDbManagerServer(databaseName: string): TDatabaseManager; static;
    class function createObjectManagerLocalParams: TObjectManager; static;
    class function createObjectManagerServer(databaseName: string): TObjectManager; static;
    class function createObjectManagerServerParams: TObjectManager; static;
    class procedure setDatabaseLocal(database: string); static;
    class procedure closeLocal; static;
    class function CreateConnection: IDBConnection;
    class function CreateFactory: IDBConnectionFactory;
    class function CreateConnectionLocal: IDBConnection;
    class function CreateFactoryLocal: IDBConnectionFactory;
    class procedure createDirectories; static;
    class function getAppdataDirPath: string; static;
    class function getDbLocalParamsPath: string; static;
    class procedure setDatabaseLocalParams; static;
    class procedure setDatabaseServer(database: string);
    class procedure setDatabaseServerParams;

  end;

var
  frmCon: TfrmCon;

implementation


{%CLASSGROUP 'System.Classes.TPersistent'}

uses
  Aurelius.Drivers.Base, uconst;

{$R *.dfm}

{ TMyConnectionModule }

class function TfrmCon.canConnectToServer: TBooleanMsg;
begin
  try
    frmCon.conServer.Connect;
    Result.success := True;
  except
    on E: Exception do
    begin
      Result.success := False;
      Result.message := e.ToString;
    end
    else
      begin
        frmCon.conServer.close;
        Result.success := True;
      end;

  end;

end;

class function TfrmCon.CreateConnection: IDBConnection;
begin
  Result := frmCon.AurConServer.CreateConnection;
end;

class function TfrmCon.CreateConnectionLocal: IDBConnection;
begin
  setDatabaseLocal(DB_LOCAL_PARAMS_NAME);
  Result := frmCon.aurConLocal.CreateConnection;
end;

class function TfrmCon.CreateFactory: IDBConnectionFactory;
begin
  Result := TDBConnectionFactory.Create(
    function: IDBConnection
    begin
      Result := CreateConnection;
    end);
end;

class function TfrmCon.CreateFactoryLocal: IDBConnectionFactory;
begin
  Result := TDBConnectionFactory.Create(
    function: IDBConnection
    begin
      Result := CreateConnectionLocal;
    end);
end;

class procedure TfrmCon.closeLocal;
begin
  frmCon.conLocal.Close;
end;

class procedure TfrmCon.openLocal;
begin
  frmCon.conLocal.Open;
end;

class procedure TfrmCon.openServer;
begin
  frmCon.conServer.Open;
end;

class function TfrmCon.CreateConnectionServer: IDBConnection;
begin
  Result := frmCon.AurConServer.CreateConnection;
end;

class function TfrmCon.createDbManagerServerParams: TDatabaseManager;
begin
  setDatabaseServer(DB_SERVER_PARAMS_NAME);
  Result := TDatabaseManager.Create(frmCon.CreateConnectionServer, TMappingExplorer.Get(DB_SERVER_PARAMS_NAME));
end;

class procedure TfrmCon.createDbServer(database: string);
begin
  setDatabaseServer('');
  frmCon.conServer.Open;
  frmCon.conServer.execSql('CREATE DATABASE IF NOT EXISTS  ' + database);
  setDatabaseServer(database);
end;

class procedure TfrmCon.createDbServerParams;
begin
  createDbServer(DB_SERVER_PARAMS_NAME);
end;

class function TfrmCon.createDbManagerLocalParams: TDatabaseManager;
begin
  setDatabaseLocalParams;
  Result := TDatabaseManager.Create(CreateConnectionLocal, TMappingExplorer.Get(DB_LOCAL_PARAMS_NAME));
end;

class function TfrmCon.createDbManagerServer(databaseName: string): TDatabaseManager;
begin
  if databaseName <> '' then
    setDatabaseServer(databaseName);
  Result := TDatabaseManager.Create(CreateConnectionServer);
end;

class procedure TfrmCon.setDatabaseLocal(database: string);
begin
  closeLocal;
  frmCon.conLocal.Database := TPath.Combine(getAppdataDirPath, database + DB_FILE_EXT);
end;

class procedure TfrmCon.setDatabaseLocalParams;
begin
  setDatabaseLocal(DB_LOCAL_PARAMS_NAME);
end;

class function TfrmCon.createObjectManagerLocalParams: TObjectManager;
begin
  setDatabaseLocal(DB_LOCAL_PARAMS_NAME);
  Result := TObjectManager.Create(CreateConnectionLocal, TMappingExplorer.Get(DB_LOCAL_PARAMS_NAME));
end;

class function TfrmCon.createObjectManagerServer(databaseName: string): TObjectManager;
begin
  if databaseName <> '' then
    setDatabaseServer(databaseName);
  Result := TObjectManager.Create(CreateConnectionServer);
end;

class function TfrmCon.createObjectManagerServerParams: TObjectManager;
begin
  setDatabaseServer(DB_SERVER_PARAMS_NAME);
  Result := TObjectManager.Create(CreateConnectionServer, TMappingExplorer.Get(DB_SERVER_PARAMS_NAME));
end;

class procedure TfrmCon.closeServer;
begin
  frmCon.conServer.Close;
end;

class procedure TfrmCon.setDatabaseServer(database: string);
begin
  closeServer;
  frmCon.conServer.Database := database;
end;

class procedure TfrmCon.setDatabaseServerParams;
begin
  setDatabaseServer(DB_SERVER_PARAMS_NAME);
end;

class procedure TfrmCon.createDirectories;
begin
  var path := TPath.Combine(TPath.GetHomePath, APPDATA_DIR);
  if not TDirectory.Exists(path) then
  try
    TDirectory.CreateDirectory(path);
  except
    on e: exception do
      raise e;
  end;
end;

class function TfrmCon.getAppdataDirPath: string;
begin
  Result := TPath.Combine(TPath.GetHomePath, APPDATA_DIR);
end;

class function TfrmCon.getDbLocalParamsPath: string;
begin
  Result := TPath.Combine(getAppdataDirPath, DB_LOCAL_PARAMS_NAME + DB_FILE_EXT);
end;



{ TMObjectManager }

procedure TMObjectManager.AddOwnershipWithSlaves<T>(var obj: T);
begin
  var i: Integer;
  var slaves := obj.getSlaves;
  Self.AddOwnership(obj);
  for i := 0 to Length(slaves) - 1 do
    if Assigned(slaves[i]) then
      Self.AddOwnership(slaves[i]);
end;

function TMObjectManager.saveIfNotExists<T>(var obj: T; cretiria: TCustomCriterion): Integer;
begin
  var o := self.Find<T>.Where(cretiria).UniqueResult;
  if o = nil then
  begin
    self.Save(obj);
    Result := obj.id;
  end
  else
  begin
    obj := o;
    Result := o.id;
  end;
end;


end.



