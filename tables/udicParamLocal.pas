unit uDicParamLocal;

interface

uses
  Aurelius.Dictionary.Classes, 
  Aurelius.Linq;

type
  TMLangDictionary = class;
  TMParamsLocalDictionary = class;
  TMTransDictionary = class;
  TMWrodDictionary = class;
  
  IMLangDictionary = interface;
  
  IMParamsLocalDictionary = interface;
  
  IMTransDictionary = interface;
  
  IMWrodDictionary = interface;
  
  IMLangDictionary = interface(IAureliusEntityDictionary)
    function Id: TLinqProjection;
    function Lang: TLinqProjection;
  end;
  
  IMParamsLocalDictionary = interface(IAureliusEntityDictionary)
    function Id: TLinqProjection;
    function Language: TLinqProjection;
  end;
  
  IMTransDictionary = interface(IAureliusEntityDictionary)
    function Id: TLinqProjection;
    function Trans: TLinqProjection;
    function Lang: IMLangDictionary;
    function Word: IMWrodDictionary;
  end;
  
  IMWrodDictionary = interface(IAureliusEntityDictionary)
    function Word: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  TMLangDictionary = class(TAureliusEntityDictionary, IMLangDictionary)
  public
    function Id: TLinqProjection;
    function Lang: TLinqProjection;
  end;
  
  TMParamsLocalDictionary = class(TAureliusEntityDictionary, IMParamsLocalDictionary)
  public
    function Id: TLinqProjection;
    function Language: TLinqProjection;
  end;
  
  TMTransDictionary = class(TAureliusEntityDictionary, IMTransDictionary)
  public
    function Id: TLinqProjection;
    function Trans: TLinqProjection;
    function Lang: IMLangDictionary;
    function Word: IMWrodDictionary;
  end;
  
  TMWrodDictionary = class(TAureliusEntityDictionary, IMWrodDictionary)
  public
    function Word: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  ILocalDictionary = interface(IAureliusDictionary)
    function MLang: IMLangDictionary;
    function MParamsLocal: IMParamsLocalDictionary;
    function MTrans: IMTransDictionary;
    function MWrod: IMWrodDictionary;
  end;
  
  TLocalDictionary = class(TAureliusDictionary, ILocalDictionary)
  public
    function MLang: IMLangDictionary;
    function MParamsLocal: IMParamsLocalDictionary;
    function MTrans: IMTransDictionary;
    function MWrod: IMWrodDictionary;
  end;
  
function DicLocalParams: ILocalDictionary;

implementation

var
  __DicLocalParams: ILocalDictionary;

function DicLocalParams: ILocalDictionary;
begin
  if __DicLocalParams = nil then __DicLocalParams := TLocalDictionary.Create;
  result := __DicLocalParams;
end;

{ TMLangDictionary }

function TMLangDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMLangDictionary.Lang: TLinqProjection;
begin
  Result := Prop('Lang');
end;

{ TMParamsLocalDictionary }

function TMParamsLocalDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMParamsLocalDictionary.Language: TLinqProjection;
begin
  Result := Prop('Language');
end;

{ TMTransDictionary }

function TMTransDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMTransDictionary.Trans: TLinqProjection;
begin
  Result := Prop('Trans');
end;

function TMTransDictionary.Lang: IMLangDictionary;
begin
  Result := TMLangDictionary.Create(PropName('Lang'));
end;

function TMTransDictionary.Word: IMWrodDictionary;
begin
  Result := TMWrodDictionary.Create(PropName('Word'));
end;

{ TMWrodDictionary }

function TMWrodDictionary.Word: TLinqProjection;
begin
  Result := Prop('Word');
end;

function TMWrodDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

{ TLocalDictionary }

function TLocalDictionary.MLang: IMLangDictionary;
begin
  Result := TMLangDictionary.Create;
end;

function TLocalDictionary.MParamsLocal: IMParamsLocalDictionary;
begin
  Result := TMParamsLocalDictionary.Create;
end;

function TLocalDictionary.MTrans: IMTransDictionary;
begin
  Result := TMTransDictionary.Create;
end;

function TLocalDictionary.MWrod: IMWrodDictionary;
begin
  Result := TMWrodDictionary.Create;
end;

end.
