unit uDicParamServer;

interface

uses
  Aurelius.Dictionary.Classes, 
  Aurelius.Linq;

type
  TMDatabaseDictionary = class;
  
  IMDatabaseDictionary = interface;
  
  IMDatabaseDictionary = interface(IAureliusEntityDictionary)
    function Id: TLinqProjection;
    function Title: TLinqProjection;
    function Descrription: TLinqProjection;
    function IsDefaultDb: TLinqProjection;
  end;
  
  TMDatabaseDictionary = class(TAureliusEntityDictionary, IMDatabaseDictionary)
  public
    function Id: TLinqProjection;
    function Title: TLinqProjection;
    function Descrription: TLinqProjection;
    function IsDefaultDb: TLinqProjection;
  end;
  
  IDBDictionary = interface(IAureliusDictionary)
    function MDatabase: IMDatabaseDictionary;
  end;
  
  TDBDictionary = class(TAureliusDictionary, IDBDictionary)
  public
    function MDatabase: IMDatabaseDictionary;
  end;
  
function DicParams: IDBDictionary;

implementation

var
  __DicParams: IDBDictionary;

function DicParams: IDBDictionary;
begin
  if __DicParams = nil then __DicParams := TDBDictionary.Create;
  result := __DicParams;
end;

{ TMDatabaseDictionary }

function TMDatabaseDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMDatabaseDictionary.Title: TLinqProjection;
begin
  Result := Prop('Title');
end;

function TMDatabaseDictionary.Descrription: TLinqProjection;
begin
  Result := Prop('Descrription');
end;

function TMDatabaseDictionary.IsDefaultDb: TLinqProjection;
begin
  Result := Prop('IsDefaultDb');
end;

{ TDBDictionary }

function TDBDictionary.MDatabase: IMDatabaseDictionary;
begin
  Result := TMDatabaseDictionary.Create;
end;

end.
