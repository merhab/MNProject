unit uDatabase;

interface

uses
  mORMot,
  System.Generics.Collections,
  System.SysUtils;

type
  TMPerson = class(TSQLRecord)
  private
    Fname: WideString;
  published
    property name: WideString read Fname write Fname;
  public
    constructor Create(name: WideString);
  end;

  TMGroup = class(TSQLRecord)
  private
    Fname: WideString;
  published
    property name: WideString read Fname write Fname;
  public
    constructor Create(name: WideString);
  end;

  TMUser = class(TMPerson)
  private
    FloginName: WideString;
    FloginPass: WideString;
  published
    FloginGroup: TMGroup;
    property loginName: WideString read FloginName write FloginName;
    property loginPass: WideString read FloginPass write FloginPass;
    property loginGroup: TMGroup read FloginGroup write FloginGroup;
  end;

  TMDatabases = class(TSQLRecord)
  private
    Fname: string;
    Ftitle: WideString;
    FisDefault: Boolean;
    function getName: string;
  published
    property title: WideString read Ftitle write Ftitle;
    property isDefault: Boolean read FisDefault write FisDefault;
  public
    property name: string read getName;
    constructor Create(title: WideString);

  end;

implementation

{ TMPerson }

constructor TMPerson.Create(name: WideString);
begin
  inherited Create;
  Self.name := name;
end;

{ TMGroup }

constructor TMGroup.Create(name: WideString);
begin
  inherited Create;
  Self.name := name;
end;

{ TMDatabases }

constructor TMDatabases.Create(title: WideString);
begin
  inherited Create;
  Self.title := title;
end;

function TMDatabases.getName: string;
begin
  Result := 'db_' + inttostr(Self.IDValue);
end;



end.

